<?php get_header("simple"); ?>
	<div class="uk-container uk-container-center">
		<div class="uk-grid uk-margin-top">
			<div class="uk-width-large-7-10">
			<?php
				if ( have_posts() ) : while (have_posts()) : the_post();
			?>
			<h1>Search Results for: <?php echo get_search_query() ?></h1>
				<?php get_template_part("loop","view"); ?>
				<?php endwhile; else : ?>
				<h1 class="uk-text-center">No Results Found!</h1>
				<?php get_search_form(); ?>
				<?php endif; ?>
				<ul class="uk-pagination uk-margin-top">
					<li class="uk-pagination-previous"><?php previous_posts_link("Newer Results"); ?></li>
					<li class="uk-pagination-next"><?php next_posts_link("Older Results"); ?></li>
				</ul>
			</div>
			<div class="uk-width-3-10">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
<?php get_footer(); ?>