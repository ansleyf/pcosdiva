<?php
	get_header("simple");
?>
	<div class="uk-container uk-container-center">
		<div class="uk-grid">
			<div class="uk-width-medium-7-10">
			 <div class="uk-article-title">
			 	<?php 
			 		if ( is_category() ) {
					single_cat_title(); }
					elseif ( is_tag() ) {
					single_tag_title(); }
				?>
			</div>
			 <?php if (is_category()) { echo category_description(); ?> <hr/> <?php } ?>
			<?php wp_reset_query(); while(have_posts()) : the_post(); ?>
				<?php get_template_part("loop","view"); ?>
			<?php endwhile; ?>
			<ul class="uk-pagination uk-margin-top">
				<li class="uk-pagination-previous"><?php previous_posts_link("Newer Entries"); ?></li>
				<li class="uk-pagination-next"><?php next_posts_link("Older Entries"); ?></li>
			</ul>
			</div>
			<div class="uk-width-3-10">
				<?php get_sidebar(); ?>
				<?php get_template_part('sidebar-blog'); ?>
			</div>
		</div>
	</div>
<?php
	get_footer(); 