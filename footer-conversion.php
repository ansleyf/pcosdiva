<footer class="uk-width-1-1 uk-margin-large-top">
	<div class="bubble">
		<div class="uk-container uk-container-center">
			<img src="<?php echo get_template_directory_uri(); ?>/images/bubble-footer_text.png" class="uk-align-center" />
			<form method="post" class="uk-form af-form-wrapper" action="http://www.aweber.com/scripts/addlead.pl"  >
				<div style="display: none;">
					<input type="hidden" name="meta_web_form_id" value="349877396" />
					<input type="hidden" name="meta_split_id" value="" />
					<input type="hidden" name="listname" value="pcosdiva1" />
					<input type="hidden" name="redirect" value="http://www.aweber.com/thankyou.htm?m=default" id="redirect_45a09680b79cfcb58b2ab7948be9179f" />
					<input type="hidden" name="meta_adtracking" value="Website_Sign_Up_Top_Bar" />
					<input type="hidden" name="meta_message" value="1" />
					<input type="hidden" name="meta_required" value="name,email" />
					<input type="hidden" name="meta_tooltip" value="" />
				</div>
				<div class="uk-grid" data-uk-grid-margin>
					<div class="uk-width-medium-1-2"><input type="text" name="name" placeholder="first name" class="uk-width-1-1 clear black" /></div>
					<div class="uk-width-medium-1-2"><input type="email" placeholder="email address" class="uk-width-1-1 clear black" name="email"/></div>
					<div class="uk-width-1-1"><input type="submit" name="submit" value="SEND ME THE GUIDE" class="uk-button uk-button-large orange" /></div>
				</div>
			</form>
		</div>
	</div>
	<div class="af">
		<div class="uk-text-center uk-text-small">
			&copy; 2014 <?php echo get_bloginfo( "name", "raw"); ?>. All Rights Reserved. &bull; <a href="http://ansleyfones.com/">Design &amp; Development by <img src="<?php echo get_template_directory_uri(); ?>/images/af-logo-very-small.png" alt="AF" /> Ansley Fones</a>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>