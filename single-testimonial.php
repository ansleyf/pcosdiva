<?php
	get_header("simple");
?>
	<div class="uk-container uk-container-center">
		<div class="uk-grid">
			<div class="uk-width-medium-7-10">
			<?php
				while(have_posts()) : the_post();
					get_template_part("single","view");
				endwhile;
			?>
				<hr class="butterfly" />
				<?php comments_template(); ?>
			</div>
			<div class="uk-width-3-10 uk-visible-large">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
<?php
	get_footer();