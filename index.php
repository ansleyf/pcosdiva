<?php
	get_header("home");
?>
<section class="uk-width-1-1 hope">
	<div class="uk-container uk-container-center">
	<h1>THERE IS HOPE</h1>
	<div class="callout">
		<p>
			Women with PCOS often feel pessimistic, confused, out of control, 
			and helpless.
		</p>
		<p>
			Good news!  There is hope.  You need not be a victim of PCOS.
			Thousands of women have reclaimed their health, and in overcoming their symptoms, found their true selves and best lives.
		</p>
		<p>
			It is time for you to do the same.
			In no time, you will learn to  manage your PCOS and thrive!
			Your first step toward taking control and transforming your life is to
			educate yourself about PCOS.
		</p>
		<h2>Start with my FREE PCOS 101 Guide to Health and Hope.</h2>
		<form method="post" class="uk-form af-form-wrapper" action="http://www.aweber.com/scripts/addlead.pl"  >
				<div style="display: none;">
					<input type="hidden" name="meta_web_form_id" value="1958810298" />
					<input type="hidden" name="meta_split_id" value="" />
					<input type="hidden" name="listname" value="pcos101guide" />
					<input type="hidden" name="redirect" value="http://www.pcosdiva.com/2013/03/almost-done-check-your-email/" id="redirect_321e8c13b711ac905e2589d212dea5ff" />
					<input type="hidden" name="meta_adtracking" value="PCOS_101_Guide_Form" />

					<input type="hidden" name="meta_message" value="1" />
					<input type="hidden" name="meta_required" value="name,email" />
					<input type="hidden" name="meta_tooltip" value="" />
				</div>
			<div class="uk-grid" data-uk-grid-margin>
				<div class="uk-width-medium-1-2 uk-margin-small-bottom"><input type="text" placeholder="first name" name="name" class="uk-width-1-1 clear white" /></div>
				<div class="uk-width-medium-1-2 uk-margin-small-bottom"><input type="email" placeholder="email address" name="email" class="uk-width-1-1 clear white" /></div>
				<div class="uk-width-1-1 uk-margin-top"><input type="submit" value="SEND ME THE GUIDE" name="submit" class="uk-button uk-button-large orange" /></div>
			</div>
		</form>
	</div>
	</div>
</section>
<section class="uk-container uk-container-center butterfly">
	<?php 
		$heading = get_post_meta(5501, "Heading");
		$column1 = get_post_meta(5501, "Column One");
		$column2 = get_post_meta(5501, "Column Two");
		$column3 = get_post_meta(5501, "Column Three");
	?>
	<h3>
		<?php echo $heading[0]; ?><br />
		<?php echo $heading[1]; ?><br />
		<?php echo $heading[2]; ?>
	</h3>
	<hr class="butterfly" />
	<h3><?php echo $heading[3]; ?></h3>
	<div class="uk-grid" data-uk-grid-margin>
		<div class="uk-width-medium-1-3">
			<div class="uk-panel uk-panel-box">
				<div class="uk-panel-teaser">
					<a href="<?php echo $column1[4]; ?>"><img src="<?php echo wp_get_attachment_url($column1[0]); ?>" /></a>
				</div>
				<h3>
					<a href="<?php echo $column1[4]; ?>"><span><?php echo $column1[1]; ?></span><br />
					<?php echo $column1[2]; ?></a>
				</h3>
				<div>
					<?php echo $column1[3]; ?>
				</div>
				<a href="<?php echo $column1[4]; ?>">LEARN MORE</a>
			</div>
		</div>
		<div class="uk-width-medium-1-3">
			<div class="uk-panel uk-panel-box">
				<div class="uk-panel-teaser">
					<a href="<?php echo $column2[4]; ?>"><img src="<?php echo wp_get_attachment_url($column2[0]); ?>" /></a>
				</div>
				<h3>
					<a href="<?php echo $column2[4]; ?>"><span><?php echo $column2[1]; ?></span><br />
					<?php echo $column2[2]; ?></a>
				</h3>
				<div>
					<?php echo $column2[3]; ?>
				</div>
				<a href="<?php echo $column2[4]; ?>">LEARN MORE</a>
			</div>
		</div>
		<div class="uk-width-medium-1-3">
			<div class="uk-panel uk-panel-box">
				<div class="uk-panel-teaser">
					<a href="<?php echo $column3[4]; ?>"><img src="<?php echo wp_get_attachment_url($column3[0]); ?>" /></a>
				</div>
				<h3>
					<a href="<?php echo $column3[4]; ?>"><span><?php echo $column3[1]; ?></span><br />
					<?php echo $column3[2]; ?></a>
				</h3>
				<div>
					<?php echo $column3[3]; ?>
				</div>
				<a href="<?php echo $column3[4]; ?>">LEARN MORE</a>
			</div>
		</div>
	</div>
</section>
<section class="about">
	<?php
		$about = get_post(5508, "ARRAY_A");
		$heading = get_post_meta($about["ID"], "Heading", true);
		$link = get_post_meta($about["ID"], "Link", true);
	?>
	<div class="uk-container uk-container-center">
		<h1><?php echo $heading; ?></h1>
		<img src="" class="uk-visible-small" /> <!-- portait for mobile -->
		<div class="uk-width-medium-1-2">
			<?php echo $about["post_content"]; ?>
			<a href="<?php echo $link; ?>">learn more</a>
		</div>
	</div>
</section>
<section class="praise">
	<div class="uk-container uk-container-center">
		<h1>PRAISE FOR PCOS DIVA</h1>
		<h3>Thousands of women have already discovered their Diva, and are thriving with PCOS</h3>
		<div class="uk-grid">
		<?php
			$args = array(
				"post_type"		=> "testimonials",
				"posts_per_page"	=> 3,
				"testimonials_category" => "private-coaching-diva"
			);
			$testi = new WP_Query($args);
			while($testi->have_posts()) : $testi->the_post();
		?>
			<div class="uk-width-medium-1-3">
				<div class="uk-panel uk-panel-box">
					<div class="uk-panel-teaser uk-vertical-align">
						<?php the_post_thumbnail("test-thumb", array("class"=>"uk-vertical-align-middle")); ?>
					</div>
					<div class="testi">
						<?php the_title(); ?>
					</div>
				</div>
			</div>
		<?php endwhile; wp_reset_postdata(); ?>
			<div class="uk-width-1-1">
				<a class="more" href="./praise/">see more praise here...</a>
			</div>
		</div>
		<hr class="butterfly" />
	</div>
</section>
<section class="uk-container uk-container-center blog">
	<div class="uk-grid">
		<div class="uk-width-medium-7-10">
		<?php wp_reset_query(); while(have_posts()) : the_post(); ?>
			<?php get_template_part("loop","view"); ?>
		<?php endwhile; ?>
		<ul class="uk-pagination uk-margin-top">
				<li class="uk-pagination-previous"><a href="./category/blog/page/2/"><i class="uk-icon uk-icon-caret-left"></i> Older Entries</a></li>
			</ul>
		</div>
		<div class="uk-width-3-10">
			<?php get_sidebar(); ?>
		</div>
	</div>
</section>
<?php
	get_footer("conversion");