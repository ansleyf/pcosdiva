<?php
	get_header("nocomp");
?>
	<div class="programs">
		<div class="top">
			<img class="uk-align-center" src="<?php echo get_template_directory_uri(); ?>/images/gs_title.png" alt="Begin Your Journey" />
		</div>
		<div class="uk-container uk-container-center fullwidth content">
			<div class="uk-width-1-1">
			<?php while(have_posts()) : the_post(); ?>
					<?php the_content(); ?>
			<?php endwhile; ?>
			</div>
			<section class="butterfly">
				<div class="uk-grid" data-uk-grid-margin>
				<?php
					$column1 = get_post_meta(5501, "Column One");
					$column2 = get_post_meta(5501, "Column Two");
					$column3 = get_post_meta(5501, "Column Three");
				?>
					<div class="uk-width-medium-1-3">
						<div class="uk-panel uk-panel-box">
							<div class="uk-panel-teaser">
								<a href="<?php echo $column1[4]; ?>"><img src="<?php echo wp_get_attachment_url($column1[0]); ?>" /></a>
							</div>
							<h3>
								<a href="<?php echo $column1[4]; ?>"><span><?php echo $column1[1]; ?></span><br />
								<?php echo $column1[2]; ?></a>
							</h3>
							<div>
								<?php echo $column1[3]; ?>
							</div>
							<a href="<?php echo $column1[4]; ?>">LEARN MORE</a>
						</div>
					</div>
					<div class="uk-width-medium-1-3">
						<div class="uk-panel uk-panel-box">
							<div class="uk-panel-teaser">
								<a href="<?php echo $column2[4]; ?>"><img src="<?php echo wp_get_attachment_url($column2[0]); ?>" /></a>
							</div>
							<h3>
								<a href="<?php echo $column2[4]; ?>"><span><?php echo $column2[1]; ?></span><br />
								<?php echo $column2[2]; ?></a>
							</h3>
							<div>
								<?php echo $column2[3]; ?>
							</div>
							<a href="<?php echo $column2[4]; ?>">LEARN MORE</a>
						</div>
					</div>
					<div class="uk-width-medium-1-3">
						<div class="uk-panel uk-panel-box">
							<div class="uk-panel-teaser">
								<a href="<?php echo $column3[4]; ?>"><img src="<?php echo wp_get_attachment_url($column3[0]); ?>" /></a>
							</div>
							<h3>
								<a href="<?php echo $column3[4]; ?>"><span><?php echo $column3[1]; ?></span><br />
								<?php echo $column3[2]; ?></a>
							</h3>
							<div>
								<?php echo $column3[3]; ?>
							</div>
							<a href="<?php echo $column3[4]; ?>">LEARN MORE</a>
						</div>
					</div>
				</div>
			</section>
			<hr class="butterfly" />
			<div class="quote">
				<div>We delight in the beauty of the butterfly,<br />
				but rarely admit the changes it has gone through to achieve that beauty</div>
				<span>- Maya Angelou</span>
			</div>
		</div>
	</div>
<?php
	get_footer("conversion");