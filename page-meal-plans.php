<?php
	get_header("nocomp");
?>
	<div class="mealplans">
		<div class="uk-container uk-container-center">
			<img class="mealplan-logo uk-align-center" src="<?php echo get_template_directory_uri(); ?>/images/mealplans-title.png" />
			<p class="intro">Using fresh, seasonal food is the cornerstone of the PCOS Diva Lifestyle! Seasonal fruits and vegetables are often less expensive and fresher! Just as the change in seasons turn in a cyclical pattern, so do our bodies. PCOS symptoms may include irregular cycles, irritability, depression, and lethargy. PCOS Diva recipes transition with the seasons, helping our bodies to return to their regular rhythm.</p>
			<div class="hero">
				<h2>6 weeks for $34</h2>
				<p>Purchase and receive your Summer Meal Plan by email immediately.</p>
				<a class="uk-button orange uk-button-large" href="/programs/meal-plans/summer-meals">Get it Now!</a>
			</div>
			<section class="plans">
				<h2>Learn more about the pcos diva meal plan recipes</h2>
				<div class="spring">
					<a href="/programs/meal-plans/spring"><img src="<?php echo get_template_directory_uri(); ?>/images/mealplans/spring.jpg"></a>
					<div class="uk-grid uk-margin-top" data-uk-grid-margin>
						<div class="uk-width-small-8-10">
							<p>Spring is the natural time to detox, and you’ll feel more energized and lighter with greens, lentils, and berries. Just as we feel lighter on a lighter day, these foods will give us the lift and energy we need.</p>
						</div>
						<div class="uk-width-small-2-10 uk-text-center"><a class="uk-button orange uk-button-large" href="/programs/meal-plans/spring">Learn More</a></div>
					</div>
				</div>
				<div class="summer">
					<a href="/programs/meal-plans/summer-meals"><img src="<?php echo get_template_directory_uri(); ?>/images/mealplans/summer.jpg"></a>
					<div class="uk-grid uk-margin-top" data-uk-grid-margin>
						<div class="uk-width-small-8-10">
							<p>Enjoy the summer sun with light and fresh ingredients such as grilled chicken, shrimp, and fresh smoothies to cool you down. We all want to be playing outside in the summer, and for women with PCOS, these cooling foods will refresh us, allowing us to get back to the things we love to do outside.</p>
						</div>
						<div class="uk-width-small-2-10 uk-text-center"><a class="uk-button orange uk-button-large" href="/programs/meal-plans/summer-meals">Learn More</a></div>
					</div>
				</div>
				<div class="fall">
					<a href="/programs/meal-plans/fall/"><img src="<?php echo get_template_directory_uri(); ?>/images/mealplans/fall.jpg"></a>
					<div class="uk-grid uk-margin-top" data-uk-grid-margin>
						<div class="uk-width-small-8-10">
							<p>In the fall, I turn towards warming autumn harvest foods including squash, sweet potatoes, carrots, parsnips, onions, and garlic. This season lends itself to preparation for the winter, and we’ll transition using foods of the harvest.</p>
						</div>
						<div class="uk-width-small-2-10 uk-text-center"><a class="uk-button orange uk-button-large" href="/programs/meal-plans/fall/">Learn More</a></div>
					</div>
				</div>
				<div class="winter">
					<a href="/programs/meal-plans/winter/"><img src="<?php echo get_template_directory_uri(); ?>/images/mealplans/winter.jpg"></a>
					<div class="uk-grid uk-margin-top" data-uk-grid-margin>
						<div class="uk-width-small-8-10">
							<p>Winter hibernation means we need more sustenance such as fats and richer roasts, stews, chili and gumbo, just spicy enough to keep us warm. These hearty foods will give us energy throughout the cold months, while reducing cravings.</p>
						</div>
						<div class="uk-width-small-2-10 uk-text-center"><a class="uk-button orange uk-button-large" href="/programs/meal-plans/winter/">Learn More</a></div>
					</div>
				</div>
				<div class="crockpot">
					<a href="/programs/meal-plans/crock-pot-recipes/"><img src="<?php echo get_template_directory_uri(); ?>/images/mealplans/crockpot.jpg"></a>
					<div class="uk-grid uk-margin-top" data-uk-grid-margin>
						<div class="uk-width-small-8-10">
							<p>Who doesn’t love the convenience of a slow cooker? These recipes have been designed to help women with PCOS feel their best by eating nutrient-rich food that takes little time and effort to prepare.</p>
						</div>
						<div class="uk-width-small-2-10 uk-text-center"><a class="uk-button orange uk-button-large" href="/programs/meal-plans/crock-pot-recipes/">Learn More</a></div>
					</div>
				</div>
			</section>
			<div class="uk-grid" data-uk-grid-margin>
				<div class="uk-width-medium-1-2 uk-push-1-2 mobile-banner">
					<img src="<?php echo get_template_directory_uri(); ?>/images/mealplans-sidebar.jpg" />
				</div>
				<div class="uk-width-medium-1-2 uk-pull-1-2 content">
				<?php while(have_posts()) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; ?>
				</div>
			</div>
		</div>
	</div>
<?php
	get_footer($footer);