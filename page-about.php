<?php
	get_header("simple");
?>
	<div class="about">
	<?php
		while(have_posts()) : the_post();
		$row1 = get_post_meta($post->ID, "Row One");
		$row2 = get_post_meta($post->ID, "Row Two");
		$footer = get_post_meta($post->ID, "Footer Type", true);
	?>

		<div class="uk-grid uk-grid-preserve about-t" data-uk-grid-match="{row: true}">
			<div class="uk-width-medium-1-2 notext uk-hidden-small" style="background: url('<?php echo wp_get_attachment_url($row1[0]); ?>');">
			</div>
			<div class="uk-width-medium-1-2">
				<div class="container">
					<h2><?php echo $row1[1]; ?></h2>
					<?php echo $row1[2]; ?>
					<div class="uk-margin-top">
						<a class="cta orange" href="<?php echo $row1[4]; ?>"><?php echo $row1[3]; ?></a>
					</div>
				</div>
			</div>
		</div>
		<div class="uk-grid uk-grid-preserve about-t uk-margin-top-remove" data-uk-grid-match="{row: true}">
			<div class="uk-width-medium-1-2 hasbg">
				<div class="container about-box-2">
					<h2><?php echo $row2[1]; ?></h2>
					<?php echo $row2[2]; ?>
					<div class="uk-margin-top">
						<a class="cta white" href="<?php echo $row2[4]; ?>"><?php echo $row2[3]; ?></a>
					</div>
				</div>
			</div>
			<div class="uk-width-medium-1-2 notext uk-hidden-small" style="background: url('<?php echo wp_get_attachment_url($row2[0]); ?>');">
			</div>
		</div>
		<div class="uk-grid about-p container uk-grid-preserve uk-margin-large-top">
			<div class="uk-width-medium-1-2 amy-profile-pic">
				<?php the_post_thumbnail("full",array("class"=>"uk-align-center")); ?>
			</div>
			<div class="uk-width-medium-1-2">
				<h1>About Amy</h1>
				<?php the_content(); ?>
			</div>
			<div id="extended-story" class="uk-width-1-1 uk-margin-large-top uk-text-center">
				<a class="cta story" data-uk-toggle="{target: '.story'}">Read Extended Story</a>
				<div class="uk-width-1-1 uk-text-left uk-hidden uk-animation-fade story">
					<?php echo get_post_meta($post->ID, "Extended Content", true); ?>
					<div class="uk-text-center uk-margin-large-top">
						<a class="cta" data-uk-toggle="{target: '.story'}">Hide Extended Story</a>
					</div>
				</div>
			</div>
		</div>
	<?php endwhile; ?>
	</div>
<?php
	get_footer($footer);