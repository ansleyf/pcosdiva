<?php
    // Menu Registration
    register_nav_menus(array(
        'primary_nav' => 'Main Header Navigation',
        'footer_nav' => 'Footer Navigation'
    ));

    // Side bar Registration
    add_action( 'widgets_init', 'regsiter_theme_sidebars' );
    function regsiter_theme_sidebars() {
        register_sidebar(
            array(
                'id' => 'primary',
                'name' => __( 'Primary' ),
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget' => '</div>',
                'before_title' => '<h3 class="widget-title">',
                'after_title' => '</h3>'
            )
        );
        register_sidebar(
            array(
                'name' => 'Jumpstart Sidebar',
                'id' => 'jumpstart',
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget' => '</div>',
                'before_title' => '<h3 class="widget-title">',
                'after_title' => '</h3>'
            )
        );
        register_sidebar(
            array(
                'name' => 'Blog Only Widgets',
                'id' => 'blog-sidebar',
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget' => '</div>',
                'before_title' => '<h3 class="widget-title">',
                'after_title' => '</h3>'
            )
        );
    }

	
    // Misc registrations
    add_theme_support('post-thumbnails');
    add_image_size("list-thumb", 264, 264, TRUE);
    add_image_size("test-thumb", 150, 150, TRUE);
    
    function custom_excerpt_length( $length ) {
		return 30;
	}
	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

    // ShortenText
	function ShortenText($text, $chars_limit = 25) { // Function name ShortenText
        $chars_text = strlen($text);
        $text = $text." ";
        $text = substr($text,0,$chars_limit);
        $text = substr($text,0,strrpos($text,' '));

        if ($chars_text > $chars_limit)
        { $text = $text."..."; } // Ellipsis
        return $text;
	}
    function related_products() {
        global $id;
        $category = get_the_category($id);
        $args = array(
            "post_type"         => "product",
            "category_name"     => $category[0]->cat_name,
            "posts_per_page"    => 3,

        );
        query_posts($args);
        echo '<ul class="related-products">';
        echo '<h2>Related Products</h2>';
        while(have_posts()) : the_post();
        echo '<li><a href="'.get_permalink().'">';
            echo '<div class="uk-display-block uk-width-1-1">';
                the_post_thumbnail(array(75,75),array("class"=>"uk-align-left"));
                the_title();
                echo '<span class="uk-display-block uk-margin-remove">';
                mp_product_price();
                echo '</span>';
            echo '</div> <div class="uk-clearfix"></div>';
        echo '</a></li>';
        endwhile;
        echo '</ul>';  
    }


// create Testimonials custom post type
add_action( 'init', 'create_testimonials' );
function create_testimonials() {
  $labels = array(
    'name' => _x('Testimonials', 'post type general name'),
    'singular_name' => _x('Testimonial', 'post type singular name'),
    'add_new' => _x('Add New', 'Testimonial'),
    'add_new_item' => __('Add New Testimonial'),
    'edit_item' => __('Edit Testimonial'),
    'new_item' => __('New Testimonial'),
    'view_item' => __('View Testimonial'),
    'search_items' => __('Search Testimonials'),
    'not_found' =>  __('No Testimonials found'),
    'not_found_in_trash' => __('No Testimonials found in Trash'),
    'parent_item_colon' => ''
  );

  $supports = array('title', 'editor', 'revisions', 'comments', 'thumbnail', 'excerpt');

  register_post_type( 'testimonials',
    array(
      'labels' => $labels,
      'public' => true,
      'supports' => $supports
    )
  );
}

// Create taxonomies for Testimonials post type
add_action( 'init', 'create_testimonials_taxonomies', 0 );
function create_testimonials_taxonomies() {

    register_taxonomy( 'testimonial-type', array( 'testimonials' ), array(
        'hierarchical' => true,
        'label' => "Types", 
        'singular_label' => "Type", 
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
    )
  );    
  
     register_taxonomy( 'options', array( 'testimonials' ), array(
        'hierarchical' => true,
        'label' => "Options", 
        'singular_label' => "Option", 
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
    )
  );
}

// Create Shortcode for Testimonials
function testimonial_function($atts, $content){
    extract(shortcode_atts(array('id' => 'random'), $atts));
    global $post;
    $args = array(
        "post_type"         => "testimonials",
        "orderby"           => "rand",
        "posts_per_page"    => 1
    );
    if($id !== "random") {
        $args['name'] = $id;
    }
    $testi = new WP_Query($args);
    if ($testi->have_posts()) {
        while ($testi->have_posts()): $testi->the_post();
            $out = '<div id="testimonial-sc">
                        <div class="uk-grid">
                            <div class="uk-width-2-5 thumb">'.get_the_post_thumbnail($post->ID, "test-thumb", array("class" => "uk-border-circle")).'</div>
                            <div class="uk-width-3-5 text"><img src="/wp-content/themes/pcosdiva/images/quote-left.png" /> '.ShortenText(get_the_excerpt(), 200).' <img src="/wp-content/themes/pcosdiva/images/quote-right.png" /></div>
                            <div class="uk-width-1-1 uk-text-right"><div class="read-more"><a href="'.get_permalink().'">Continue Reading</a></div></div>
                        </div>
                    </div>';
        endwhile;
    } else {
        return;
    }
    wp_reset_query();
    return $out;
}

function register_shortcodes(){
   add_shortcode('testimonial', 'testimonial_function');
}
add_action( 'init', 'register_shortcodes');

// ======== Add taxonomy filter to admin when viewing Testimonials =======
function restrict_testimonials_by_section() {
        global $typenow;
        $post_type = 'testimonials'; // change HERE
        $taxonomy = 'testimonial-type'; // change HERE
        if ($typenow == $post_type) {
            $selected = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
            $info_taxonomy = get_taxonomy($taxonomy);
            wp_dropdown_categories(array(
                'show_option_all' => __("Show All {$info_taxonomy->label}"),
                'taxonomy' => $taxonomy,
                'name' => $taxonomy,
                'orderby' => 'name',
                'selected' => $selected,
                'show_count' => true,
                'hide_empty' => true,
            ));
        };
    }

    add_action('restrict_manage_posts', 'restrict_testimonials_by_section');

    function convert_id_to_term_in_query($query) {
        global $pagenow;
        $post_type = 'testimonials'; // change HERE
        $taxonomy = 'testimonial-type'; // change HERE
        $q_vars = &$query->query_vars;
        if ($pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0) {
            $term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
            $q_vars[$taxonomy] = $term->slug;
        }
    }

    add_filter('parse_query', 'convert_id_to_term_in_query');
    
// custom columns
add_filter("manage_edit-testimonials_columns", "testimonials_columns");
add_action("manage_posts_custom_column", "testimonials_custom_columns",10,2);

function testimonials_columns($columns){
    $columns = array(
        "cb" => "<input type=\"checkbox\" />",
        "title" => "Title",
        "slug" => "URL Slug",
        "testimonials-type" => "Category"
    );
    return $columns;
}

function testimonials_custom_columns($column,$id) {
    global $wpdb;
        switch ($column) {
        case 'testimonials-type':
            $types = $wpdb->get_results("SELECT name FROM $wpdb->posts LEFT OUTER JOIN $wpdb->term_relationships ON ID = object_id LEFT OUTER JOIN $wpdb->terms ON term_taxonomy_id = term_id WHERE ID = {$id}");
            foreach($types as $loopId => $type) {
                echo $type->name.', ';
            }
            break;
        case 'slug':
            $text = basename(get_post_permalink($id));
            echo $text;
            break;
        default:
            break;
        } // end switch
}



    // Menu Walker Class
    class themeslug_walker_nav_menu extends Walker_Nav_Menu {
    // add classes to ul sub-menus
    function start_lvl( &$output, $depth ) {
        // depth dependent classes
        $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
        $display_depth = ( $depth + 1); // because it counts the first submenu as 0
        $classes = array(
            'sub-menu uk-nav uk-nav-dropdown',
            ( $display_depth % 2  ? 'menu-odd' : 'menu-even' ),
            ( $display_depth >=2 ? 'sub-sub-menu' : '' ),
            'menu-depth-' . $display_depth
            );
        $class_names = implode( ' ', $classes );

        // build html
        $output .= "\n" . $indent . '<div class="uk-dropdown uk-dropdown-navbar"><ul class="' . $class_names . '">' . "\n";
    }

    // add main/sub classes to li's and links
     function start_el( &$output, $item, $depth, $args ) {
        global $wp_query;
        $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

        // depth dependent classes
        $depth_classes = array(
            ( $depth == 0 ? 'main-menu-item' : 'sub-menu-item' ),
            ( $depth >=2 ? 'sub-sub-menu-item' : '' ),
            ( $depth % 2 ? 'menu-item-odd' : 'menu-item-even' ),
            'menu-item-depth-' . $depth
        );
        $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );

        // passed classes
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );
        if(strpos($class_names, 'menu-item-has-children')) {$haschild = "data-uk-dropdown"; }
        // build html
        $output .= $indent . '<li '. $haschild .' id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';

        // link attributes
        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
        $attributes .= ' class="menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';

        $item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
            $args->before,
            $attributes,
            $args->link_before,
            apply_filters( 'the_title', $item->title, $item->ID ),
            $args->link_after,
            $args->after
        );

        // build html
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}