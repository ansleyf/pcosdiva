<?php
    $menuargs = array(
        "container"         => "",
        "theme_location"    => "primary_nav",
        "menu_class"        => "uk-navbar-nav uk-visible-large",
        "menu_id"           => "",
        'walker'            => new themeslug_walker_nav_menu
    );
    $offcanvnav = array(
        "container"         => "",
        "theme_location"    => "primary_nav",
        "menu_class"        => "uk-nav uk-nav-offcanvas",
        "menu_id"           => ""
	);
?>
<header>
	<div id="offcanv" class="uk-offcanvas">
		<div class="uk-offcanvas-bar">
			<?php wp_nav_menu($offcanvnav); ?>
		</div>
	</div>
	<nav class="uk-navbar uk-navbar-attached uk-width-1-1">
		<div class="uk-container uk-container-center">
			<a href="#offcanv" class="uk-navbar-toggle uk-margin-right uk-hidden-large" data-uk-offcanvas></a>
			<div class="uk-navbar-content uk-align-right uk-align-medium-left">
				<a href="https://www.facebook.com/pcosdiva" target="_blank"><i class="uk-icon uk-icon-facebook uk-icon-button"></i></a>
				<a href="https://twitter.com/pcosdiva" target="_blank"><i class="uk-icon uk-icon-twitter uk-icon-button" ></i></a>
				<a href="http://instagram.com/pcosdiva" target="_blank"><i class="uk-icon uk-icon-instagram uk-icon-button"></i></a>
				<a href="http://www.pinterest.com/pcosdiva/" target="_blank"><i class="uk-icon uk-icon-pinterest uk-icon-button"></i></a>
				<a href="<?php bloginfo('rss2_url'); ?>"><i class="uk-icon uk-icon-rss uk-icon-button"></i></a>
			</div>
			<div class="uk-navbar-center">
				<?php wp_nav_menu($menuargs); ?>
			</div>
			<div class="uk-navbar-content uk-align-left uk-align-medium-right">
				<?php get_search_form(); ?>
			</div>
		</div>
	</nav>