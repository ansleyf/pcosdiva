<?php
	get_header("simple");
?>
	<div class="uk-container uk-container-center praise">
		<div class="uk-grid">
			<div class="uk-width-large-7-10">
			<?php
				$args = array(
					"post_type"		=> "testimonials",
					"posts_per_page"	=> -1
				);
				$testi = new WP_Query($args);
				while($testi->have_posts()) : $testi->the_post();
			?>
				<?php get_template_part("praiseloop","view"); ?>
			<?php endwhile; ?>
			</div>
			<div class="uk-width-3-10 uk-visible-large">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
<?php
	get_footer(); 