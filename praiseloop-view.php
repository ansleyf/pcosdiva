<article class="uk-article">
	<a href="<?php the_permalink(); ?>">
		<?php the_post_thumbnail("test-thumb", array("class"=>"uk-align-left uk-border-circle")); ?>
	</a>
	<div class="article-text">
		<a href="<?php the_permalink(); ?>">
			<div class="uk-article-title"><?php the_title(); ?></div>
		</a>
	</div>
</article>