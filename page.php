<?php
	get_header("simple");
	$footer = get_post_meta($post->ID, "Footer Type", true);
?>
	<div class="uk-container uk-container-center">
	<div class="uk-grid">
		<div class="uk-width-small-7-10">
		<?php while(have_posts()) : the_post(); ?>
			<article class="uk-article">
				<h1 class="uk-article-title"><?php the_title(); ?></h1>
				<?php the_content(); ?>
			</article>
		<?php endwhile; ?>
		</div>
		<div class="uk-width-3-10">
			<?php get_sidebar(); ?>
		</div>
	</div>
	</div>
<?php
	get_footer($footer);