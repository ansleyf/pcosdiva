<?php
	get_header("nocomp");
?>
	<div class="jumpstart">
		<div class="uk-grid">
			<div class="uk-width-medium-1-2 hasbg">
				<div class="container">
					<?php get_sidebar("jumpstart"); ?>
				</div>
			</div>
			<div class="uk-width-1-2 jumpstart-main">
				<img class="uk-width-1-1 jumpstart-logo" src="<?php echo get_template_directory_uri(); ?>/images/jumpstart-title.png" />
				<?php while(have_posts()) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
<?php
	get_footer($footer);