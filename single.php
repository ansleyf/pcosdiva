<?php
	get_header("simple");
?>
	<div class="uk-container uk-container-center">
		<div class="uk-grid">
			<div class="uk-width-medium-7-10">
			<?php
				while(have_posts()) : the_post();
					get_template_part("single","view");
				endwhile;
			?>
				<div class="post hero">
					<img class="uk-align-center" src="<?php echo get_template_directory_uri(); ?>/images/post-hero_title.png" />
					<h3>GET MY FREE EMAIL UPDATES!</h3>
					<form method="post" class="uk-form af-form-wrapper" action="http://www.aweber.com/scripts/addlead.pl"  >
						<div style="display: none;">
							<input type="hidden" name="meta_web_form_id" value="349877396" />
							<input type="hidden" name="meta_split_id" value="" />
							<input type="hidden" name="listname" value="pcosdiva1" />
							<input type="hidden" name="redirect" value="http://www.aweber.com/thankyou.htm?m=default" id="redirect_45a09680b79cfcb58b2ab7948be9179f" />
							<input type="hidden" name="meta_adtracking" value="Website_Sign_Up_Top_Bar" />
							<input type="hidden" name="meta_message" value="1" />
							<input type="hidden" name="meta_required" value="name,email" />
							<input type="hidden" name="meta_tooltip" value="" />
						</div>
						<div class="uk-grid" data-uk-grid-margin>
							<div class="uk-width-medium-1-2"><input type="text" placeholder="first name" name="name" class="uk-width-1-1 clear white" /></div>
							<div class="uk-width-medium-1-2"><input type="email" placeholder="email address" name="email" class="uk-width-1-1 clear white" /></div>
							<div class="uk-width-1-1"><input type="submit" value="Subscribe" name="submit" class="uk-button uk-button-large orange" /></div>
						</div>
					</form>
				</div>
				<?php related_posts() ?>
				<hr class="butterfly" />
				<?php comments_template(); ?>
			</div>
			<div class="uk-width-3-10">
				<?php get_sidebar(); ?>
				<?php get_template_part('sidebar-blog'); ?>
			</div>
		</div>
	</div>
<?php
	get_footer();