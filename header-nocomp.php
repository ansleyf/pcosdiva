<!DOCTYPE html>
<html>
    <head>
    	<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <title><?php wp_title( '|', true, 'right' ); ?><?php echo get_bloginfo( "name", "raw"); ?></title>

		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

        <link href="<?php echo get_template_directory_uri(); ?>/css/uikit.almost-flat.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_template_directory_uri(); ?>/css/addons/uikit.almost-flat.addons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_template_directory_uri(); ?>/style.css" rel="stylesheet" type="text/css" />
        
        <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico" />
       
	 	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/uikit.min.js"></script>

		<script type="text/javascript" src="//use.typekit.net/ori3xdb.js"></script>
		<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
    	<?php get_template_part("common", "header"); ?>
		<div class="uk-width-1-1 site-header tiny">
			<div class="uk-container uk-container-center">
				<div class="uk-grid">
					<div class="uk-width-medium-1-2"><a href="<?php echo home_url(); ?>"><img class="logo" src="<?php echo get_template_directory_uri(); ?>/images/logo-tiny.png" /></a></div>
					<div class="uk-width-1-2 uk-visible-large"><img src="<?php echo get_template_directory_uri(); ?>/images/head-tiny_right.png" /></div>
				</div>
			</div>
		</div>
    </header>