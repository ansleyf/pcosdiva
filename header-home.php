<!DOCTYPE html>
<html>
    <head>
    	<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <title><?php wp_title( '|', true, 'right' ); ?><?php echo get_bloginfo( "name", "raw"); ?></title>

		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

        <link href="<?php echo get_template_directory_uri(); ?>/css/uikit.almost-flat.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_template_directory_uri(); ?>/css/addons/uikit.almost-flat.addons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_template_directory_uri(); ?>/style.css" rel="stylesheet" type="text/css" />
        
        <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico" />
       
	 	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/uikit.min.js"></script>

		<script type="text/javascript" src="//use.typekit.net/ori3xdb.js"></script>
		<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
		<?php get_template_part("common", "header"); ?>
		<div class="uk-width-1-1 site-header home">
			<a href="<?php echo home_url(); ?>"><img class="logo uk-visible-large" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" /></a>
			<a href="<?php echo home_url(); ?>"><img class="logo uk-hidden-large" src="<?php echo get_template_directory_uri(); ?>/images/logo-tiny.png" /></a>
			<div class="uk-container uk-container-center">
				<div class="callout uk-visible-large">
					<p>
						You have the power to reclaim your life.<br />
						Take the first steps to live your best life and thrive with PCOS.<br />
						You are worth it, and PCOS Diva can help.<br />
					</p>
					<a class="uk-button uk-button-large orange" href="/programs">LET'S BEGIN</a>
				</div>
			</div>
		</div>
    </header>