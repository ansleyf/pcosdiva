<!DOCTYPE html>
<html>
    <head>
    	<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <title><?php wp_title( '|', true, 'right' ); ?><?php echo get_bloginfo( "name", "raw"); ?></title>

		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

        <link href="<?php echo get_template_directory_uri(); ?>/css/uikit.almost-flat.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_template_directory_uri(); ?>/css/addons/uikit.almost-flat.addons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo get_template_directory_uri(); ?>/style.css" rel="stylesheet" type="text/css" />
        
        <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico" />
       
	 	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/uikit.min.js"></script>

		<script type="text/javascript" src="//use.typekit.net/ori3xdb.js"></script>
		<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
    	<?php get_template_part("common", "header"); ?>
		<div class="uk-width-1-1 site-header small">
			<a href="<?php echo home_url(); ?>"><img class="logo uk-visible-large" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" /></a>
			<a href="<?php echo home_url(); ?>"><img class="logo uk-hidden-large" src="<?php echo get_template_directory_uri(); ?>/images/logo-tiny.png" /></a>
			<div class="subscribe uk-hidden-small">
				<div class="uk-container uk-container-center">
					<div class="uk-grid">
						<div class="uk-width-medium-1-2">
							<h2>Get my FREE PCOS 101 Guide to Health and Hope</h2>
						</div>
						<div class="uk-width-medium-1-2">
							<form method="post" class="uk-form af-form-wrapper" action="http://www.aweber.com/scripts/addlead.pl"  >
								<div style="display: none;">
									<input type="hidden" name="meta_web_form_id" value="349877396" />
									<input type="hidden" name="meta_split_id" value="" />
									<input type="hidden" name="listname" value="pcosdiva1" />
									<input type="hidden" name="redirect" value="http://www.aweber.com/thankyou.htm?m=default" id="redirect_45a09680b79cfcb58b2ab7948be9179f" />
									<input type="hidden" name="meta_adtracking" value="Website_Sign_Up_Top_Bar" />
									<input type="hidden" name="meta_message" value="1" />
									<input type="hidden" name="meta_required" value="name,email" />
									<input type="hidden" name="meta_tooltip" value="" />
								</div>
								<input type="text" name="name" placeholder="first name" class="uk-display-inline uk-margin-right clear white" />
								<input type="email" name="email" placeholder="email address" class="uk-display-inline uk-margin-right clear white" />
								<input type="submit" name="submit" value="YES!" class="uk-button uk-display-inline orange small" />
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
    </header>